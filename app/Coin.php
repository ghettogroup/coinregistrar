<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coin extends Model
{
    protected $table = "coins";

    protected $guarded = ["created_at", "updated_at", "deleted_at"];

    public function users()
    {
        return $this->belongsToMany(User::class, "coin_user");
    }

}
