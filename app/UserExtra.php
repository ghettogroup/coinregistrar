<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserExtra extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'date_of_birth',
        'city',
        'state',
        'country',
        'zip_postal_code',
        'mobile_phone_number',
        'skype_name',
        'discord_name',
        'language',
        'passport_id',
        'passport_photo',
        'country_id_card_no',
        'country_id_card',
        'driver_licence_no',
        'driver_licence',
        'photo',
    ];


    public function user() {
        return $this->belongsTo(User::class);
    }
}
