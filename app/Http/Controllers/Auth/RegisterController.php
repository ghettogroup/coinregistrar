<?php

namespace App\Http\Controllers\Auth;

use App\File;
use App\User;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    protected $redirectTo = '/thanks';

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm()
    {
        if(auth()->check()) {
            return redirect()->to('/register-coin');
        }
        return view('layouts.register_1');
    }

    public function register(Request $request)
    {

        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

//        $this->guard()->login($user);

        return $this->registered($request, $user)
            ?: redirect($this->redirectPath());
    }

    protected function validator(array $data)
    {

        return Validator::make($data, [
            'fullname' => ['required', 'max:255', 'min:3'],
            'username' => ['required', 'min:3'],
            'email' => ['required', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'min:8', 'confirmed'],
            'g-recaptcha-response' => ['required']
        ],[
            'fullname.required' => 'Full name is required',
            'fullname.max'      => 'Full name cannot be more than 255 characters',
            'fullname.min'      => 'Full name cannot be less than 3 characters',
            'username.required' => 'Username is required',
            'username.min'      => 'Username cannot be less than 3 characters',
            'email.rewuired'    => 'E-mail is required',
            'email.max'         => 'E-mail cannot be more than 255 charecters',
            'email.email'       => 'Please enter a valid e-mail',
            'email.unique'      => 'This e-mail is already registered',
            'password.required' => 'Password is required',
            'password.min'      => 'Password must be at least 8 characters',
            'password.confirmed' => 'Your password must match the confirmation',
            'g-recaptcha-response.required' => 'Please check the recaptcha button!'
        ]);
    }

    protected function create(array $data)
    {
        $user = User::create([
            'fullname' => $data['fullname'],
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);

        $data = [
            "fullname" => $user->fullname,
            "token" => encrypt($user->id),
        ];

        Mail::to($user->email)->send(new \App\Mail\RegisterFirst($data));

        return $user;
    }

    public function part2($encrypted_id = null)
    {
        if(!$encrypted_id) {
            abort(404);
        }

        $user_id = decrypt($encrypted_id);

        $user = User::find($user_id);

        if(!$user) {
            abort(404);
        }

        if($user->email_verified_at) {
            return redirect()->to("/")->withErrors("Your account already exists! Please login to your account.");
        }

        return view("layouts.register_2", compact("user"));
    }

    public function completeRegister(Request $request)
    {
        $this->validate($request,[
            "city"              => "required|min:2",
            "state"             => "required|min:2",
            "country"           => "required|min:2",
            "zip_postal_code"   => "required",
            "mobile_phone_number" => "required",
            "skype_name"        => "required",
            "discord_name"      => "required",
            "language"          => "required",
            "passport_id"       => "required",
            "passport_photo"    => "required|mimes:pdf,png,jpg,jpeg",
            "country_id_card_no" => "required",
            "country_id_card"   => "required|mimes:pdf,png,jpg,jpeg",
            "driver_licence_no" => "required",
            "driver_licence"    => "required|mimes:pdf,png,jpg,jpeg",
            "photo"             => "required|mimes:pdf,png,jpg,jpeg",
        ], [
            "city.required"      => "City is required",
            "city.min"           => "City must be at least 2 characters",
            "state.required"     => "State is required",
            "state.min"          => "State must be at least 2 characters",
            "country.required"   => "Country is required",
            "country.min"        => "Country must be at least 2 characters",
            "zip_postal_code.required"     => "Zip/Postal Code is required",
            "mobile_phone_number.required"     => "Mobile Phone Number is required",
            "skype_name.required"     => "Skype Username is required",
            "discord_name.required"     => "Discord username is required",
            "language.required"     => "Language is required",
            "passport_id.required"     => "Passport ID is required",
            "country_id_card_no.required"     => "Country Card ID is required",
            "driver_licence_no.required"     => "Driver Licence number is required",
            "passport_photo.required"     => "Passport photo is required",
            "passport_photo.mimes"     => "Passport photo can only be in pdf,png,jpg,jpeg types",
            "country_id_card.required"     => "Country ID Card is required",
            "country_id_card.mimes"     => "Country ID Card can only be in pdf,png,jpg,jpeg types",
            "driver_licence.required"     => "Driver Licence is required",
            "driver_licence.mimes"     => "Driver Licence can only be in pdf,png,jpg,jpeg types",
            "photo.required"     => "Self photo is required",
            "photo.mimes"     => "Self photo can only be in pdf,png,jpg,jpeg types",
        ]);

        $extras = $request->all();
        $files = request()->files;

        $user_id = decrypt($extras["usr_tkn"]);
        $user = User::find($user_id);

        $uploaded = [];

        foreach($files as $key => $file){
            $filename = time() . '_' .$file->getClientOriginalName();
            $size = $file->getSize();

            $new_file = File::create([
                "filename"  => $filename,
                "path"      => 'uploads/' . $filename,
                "size"      => $size
            ]);

            if(!file_exists(public_path("uploads"))) {
                mkdir(public_path("uploads"));
            }

            $destinationPath = 'uploads';
            $file->move($destinationPath,$filename);

            $uploaded[$key] = $new_file->id;
        }

        if(isset($extras["usr_tkn"])) {
            unset($extras["usr_tkn"]);
        } else {
            abort(404);
        }

        $user->extras()->create(array_merge($extras, $uploaded));

        $user->email_verified_at = Carbon::now();
        $user->save();

        $this->guard()->login($user);

        return redirect()->to("/register-coin");
    }
}
