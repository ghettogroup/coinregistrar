<?php

namespace App\Http\Controllers;

use App\Coin;
use App\File;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class CoinRegister extends Controller
{
    public function index()
    {
        return view("layouts.coin_register");
    }

    public function post(Request $request)
    {
        if(! auth()->check()) {
            return redirect()->to("/");
        }

//        if($request->get("g-recaptcha-response") === null) {
//            return redirect()->back()->withErrors("Please check the reCapctha button!");
//        }

        if($request->get("coin_id")) {
            DB::table("coin_user")->insert([
               "user_id" => auth()->id(),
               "coin_id" => $request->get("coin_id")
            ]);

            $coin = Coin::find($request->get("coin_id"));

            $data["Coin Name"] = $coin->name;
            $data["Developer"] = auth()->user()->fullname;

            Mail::to(env("COIN_REGISTRER_MAIL"))->send(new \App\Mail\MailSender($data));
        } else {
            $this->validate($request, [
                "name"              => "required|min:2",
                "type"              => "required|min:2",
                "supply"            => "required|min:2",
                "github_link"       => "required",
                "ticker"            => "required",
                "market_cap"        => "required",
                "assets"            => "required",
                "twitter"           => "required",
                "base_chain"        => "required",
                "pow_pos"           => "required",
                "exchanges"         => "required",
                "discord"           => "required",
                "logo_link"         => "required",
                "logo"              => "required|mimes:pdf,png,jpg,jpeg",
                "coin_info"         => "required|mimes:pdf,png,jpg,jpeg",
//                'g-recaptcha-response' => 'required'
            ], [
                "name.required"         => "Coin name is required",
                "name.min"              => "Coin name must be at least 2 characters",
                "type.required"         => "Coin type is required",
                "type.min"              => "Coin type must be at least 2 characters",
                "supply.required"       => "Max coin supply is required",
                "supply.min"            => "Max coin supply must be at least 2 characters",
                "github_link.required"  => "GitHub Link is required",
                "ticker.required"       => "Ticker is required",
                "market_cap.required"   => "Marketcap is required",
                "assets.required"       => "Assets are required",
                "twitter.required"      => "Twitter is required",
                "base_chain.required"   => "Base chain is required",
                "pow_pos.required"      => "PoW, PoS, Masternode or Staking is required",
                "exchanges.required"    => "Trading Exchanges is required",
                "discord.required"      => "Discord permanent Invite address is required",
                "logo_link.required"    => "Link to Logo is required",
                "logo.required"         => "Logo is required",
                "logo.mimes"            => "Logo can only be in pdf,png,jpg,jpeg types",
                "coin_info.required"    => "Coin info is required",
                "coin_info.mimes"       => "Coin info can only be in pdf,png,jpg,jpeg types",
//                'g-recaptcha-response.required' => 'Please check the recaptcha button!'
            ]);

            $coin = $request->all();
            $files = request()->files;

            if(isset($coin["coin_id"])) {
                unset($coin["coin_id"]);
            }

            $user = User::find(decrypt($coin["usr"]));

            $uploaded = []; $files_arr = [];
            foreach($files as $key => $file){
                $filename = time() . '_' .$file->getClientOriginalName();
                $size = $file->getSize();

                $file_dir = 'uploads/coins/'. str_slug($coin["name"]) . '/' ;

                $new_file = File::create([
                    "filename"  => $filename,
                    "path"      => $file_dir . $filename,
                    "size"      => $size
                ]);

                if(!file_exists(public_path("uploads/coins"))) {
                    mkdir(public_path("uploads/coins"));
                }

                if(!file_exists(public_path($file_dir))) {
                    mkdir(public_path($file_dir));
                }

                $destinationPath = $file_dir;
                $file->move($destinationPath,$filename);

                $uploaded[$key] = $new_file->id;
                $files_arr[$key] = $new_file->path;
            }

            if(isset($coin["usr"])) {
                unset($coin["usr"]);
            } else {
                abort(404);
            }

            unset($coin["_token"], $coin["usr"], $coin["coin_id"], $coin["g-recaptcha-response"]);

            $coin["Developer"] = auth()->user()->fullname;

            Mail::to(env("COIN_REGISTRER_MAIL"))->send(new \App\Mail\MailSender(array_merge($coin, $files_arr)));

            if(isset($coin["magnaex"]) && $coin["magnaex"]) {
                Mail::to(env("MAGNAEX_MAIL"))->send(new \App\Mail\MailSender(array_merge($coin, $files_arr)));
            }

            unset($coin["Developer"]);

            $c = Coin::create(array_merge($coin, $uploaded));
            DB::table("coin_user")->insert([
                "coin_id" => $c->id,
                "user_id" => auth()->id()
            ]);
        }

        return redirect()->to("/coin-registered");
    }

    public function checkCoin(Request $request)
    {
        $coin = Coin::where("name", $request->get("coin_name"))->with("users")->first();

        $coininfo = [];
        if($coin) {
            $coininfo = [
                "count" => $coin->users()->count(),
                "logo"  => File::find($coin->logo)->path,
                "id"    => $coin->id
            ];
        }

        return response()->json($coininfo);
    }
}
