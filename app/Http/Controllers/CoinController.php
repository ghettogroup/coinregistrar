<?php

namespace App\Http\Controllers;

use App\Coin;
use Illuminate\Http\Request;

class CoinController extends Controller
{
    public function index()
    {
        $coins = Coin::all();

        return view("layouts.coin_list", compact("coins"));
    }

    public function show($name)
    {
        $coin = Coin::where("name", $name)->first();

        if( !$coin ) {
            abort(404);
        }

        $logo = \App\File::find($coin->logo)->path;

        return view("layouts.show_coin", compact("coin","logo"));
    }
}
