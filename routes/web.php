<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ["uses" => "Auth\LoginController@showLoginForm"])->name("login.show");
Route::post("/login", ["uses" => "Auth\LoginController@login"])->name("login");
Route::get('/register', ["uses" => "Auth\RegisterController@showRegistrationForm"])->name("register_1");
Route::get('/forgot-password', ["uses" => "Auth\ForgotPasswordController@showLinkRequestForm"])->name("forgot_password");
Route::get('/logout', ['uses' => 'Auth\LoginController@logout']);

Route::get('/complete-registration/{id}', ['uses' => 'Auth\RegisterController@part2']);
Route::post('complete-register', ['uses' => 'Auth\RegisterController@completeRegister'])->name("complete-register");

Route::view("/disclaimer", "layouts.disclaimer")->name("disclaimer");
Route::view("/privacy-policy", "layouts.privacy_policy")->name("privacy_policy");
Route::view("/privacy-regulations", "layouts.privacy_regulations")->name("privacy_regulations");
Route::view("/faq", "layouts.faq")->name("faq");

Route::group(["middleware" => "auth"], function () {
    Route::post("/post-coin", ["uses" => "CoinRegister@post"])->name("post_coin");
    Route::get("/register-coin", ["uses" => "CoinRegister@index"]);
    Route::view("/coin-registered", "layouts.coin_thanks");
    Route::post("/check-coin", ["uses" => "CoinRegister@checkCoin"]);
    Route::get("/coin-listing", ["uses" => "CoinController@index"])->name("coin_list");
    Route::get("/coin/{name}", ["uses" => "CoinController@show"]);
    Route::get("developer-list", ["uses" => "DeveloperController@index"])->name("dev_list");
    Route::get("developer/{id}", ["uses" => "DeveloperController@show"]);
});

Auth::routes();

Route::view('/thanks', 'layouts.thanks');
