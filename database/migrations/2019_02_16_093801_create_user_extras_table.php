<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserExtrasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_extras', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned()->index();
            $table->date('date_of_birth')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('country');
            $table->string('zip_postal_code');
            $table->string('mobile_phone_number');
            $table->string('skype_name');
            $table->string('discord_name');
            $table->string('language');
            $table->string('passport_id');
            $table->integer('passport_photo')->unsigned();
            $table->string('country_id_card_no');
            $table->integer('country_id_card')->unsigned();
            $table->string('driver_licence_no');
            $table->integer('driver_licence')->unsigned();
            $table->integer('photo')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_extras');
    }
}
