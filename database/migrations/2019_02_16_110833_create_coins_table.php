<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type');
            $table->string('supply');
            $table->string('github_link');
            $table->string('ticker');
            $table->string('market_cap');
            $table->string('bitcoin_talk');
            $table->string('assets');
            $table->string('twitter');
            $table->string('base_chain');
            $table->string('pow_pos');
            $table->string('exchanges');
            $table->string('discord');
            $table->string('logo_link');
            $table->integer('logo')->unsigned();
            $table->integer('magnaex')->unsigned()->nullable();
            $table->integer('coin_info')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coins');
    }
}
