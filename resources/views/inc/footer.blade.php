<footer class="footer_area footer_area_six f_bg">
    <div class="footer_bottom">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 col-md-5 col-sm-6">
                    <p class="mb-0 f_300">Desing by <a href="#">GGDev</a></p>
                </div>
                <div class="col-lg-4 col-md-3 col-sm-6">
                    {{--<div class="f_social_icon_two text-center">--}}
                        {{--<a href="#"><i class="ti-facebook"></i></a>--}}
                        {{--<a href="#"><i class="ti-twitter-alt"></i></a>--}}
                        {{--<a href="#"><i class="ti-vimeo-alt"></i></a>--}}
                        {{--<a href="#"><i class="ti-pinterest"></i></a>--}}
                    {{--</div>--}}
                </div>
                <div>
                    <ul class="list-unstyled f_menu text-center text-white">
                        <li><a href="{!! route("disclaimer") !!}">Disclaimer</a></li>
                        <li><a href="{!! route("privacy_policy") !!}">Privacy Policy</a></li>
                        <li><a href="{!! url("3A_Your_Privacy_Regulations.pdf") !!}" target="_blank">Privacy Regulations</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
