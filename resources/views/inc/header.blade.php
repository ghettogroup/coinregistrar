<header class="header_area header_area_three header_area_five">
    <nav class="navbar navbar-expand-lg menu_one menu_four">
        <div class="container">
            <a class="navbar-brand sticky_logo" href="{!! url("/") !!}">
                <img src="{!! asset("img/logos/DevRegLogoLight4x_Dark_back_ground..png") !!}" srcset="{!! asset("img/logos/DevRegLogoLight4x_Dark_back_ground..png") !!} 2x" alt="logo">
                <img src="{!! asset("img/logos/DevRegLogoLight4x_Dark_back_ground..png") !!}" srcset="{!! asset("img/logos/DevRegLogoLight4x_Dark_back_ground..png") !!} 2x" alt="">
            </a>
            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="menu_toggle">
                        <span class="hamburger">
                            <span></span>
                            <span></span>
                            <span></span>
                        </span>
                        <span class="hamburger-cross">
                            <span></span>
                            <span></span>
                        </span>
                    </span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto menu">
                    {{--<li class="nav-item">
                        <a class="nav-link" href="{!! url("/") !!}">Home</a>
                    </li>--}}
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route("faq") !!}">FAQ</a>
                    </li>
                    @guest
                    {{--<li class="nav-item">--}}
                        {{--<a class="nav-link" href="{!! route("login") !!}">Sign In</a>--}}
                    {{--</li>--}}
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route("register") !!}">Sign Up</a>
                    </li>
                    @endguest
                    @auth
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route("coin_list") !!}">Coin List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! route("dev_list") !!}">Developer List</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! url("register-coin") !!}">Register Coin</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{!! url("logout") !!}">Logout</a>
                    </li>
                    @endauth
                </ul>
            </div>
            {{--<a class="btn_get btn_hover hidden-sm hidden-xs" href="#get-app">Sign In</a>--}}
        </div>
    </nav>
</header>
