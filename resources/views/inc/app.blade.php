<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{!! asset('img/favicon.png') !!}" type="image/x-icon">
    <title>Coinregistrer</title>
    <link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendors/themify-icon/themify-icons.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendors/flaticon/flaticon.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendors/animation/animate.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendors/owl-carousel/assets/owl.carousel.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('vendors/magnify-pop/magnific-popup.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/style.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/responsive.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/validationEngine.jquery.css') !!}">
    <link rel="stylesheet" href="{!! asset('css/coinregistrer.css') !!}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
    @stack("styles")
</head>

<body>
@include("inc.header")
@yield("content")
@include("inc.footer")

<script src="{!! asset('js/jquery-3.2.1.min.js') !!}"></script>
<script src="{!! asset('js/propper.js') !!}"></script>
<script src="{!! asset('js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('vendors/wow/wow.min.js') !!}"></script>
<script src="{!! asset('vendors/sckroller/jquery.parallax-scroll.js') !!}"></script>
<script src="{!! asset('vendors/owl-carousel/owl.carousel.min.js') !!}"></script>
<script src="{!! asset('vendors/imagesloaded/imagesloaded.pkgd.min.js') !!}"></script>
<script src="{!! asset('vendors/isotope/isotope-min.js') !!}"></script>
<script src="{!! asset('vendors/magnify-pop/jquery.magnific-popup.min.js') !!}"></script>
<script src="{!! asset('js/plugins.js') !!}"></script>
<script src="{!! asset('js/main.js') !!}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"></script>
<script src="{!! asset('js/jquery.validationEngine.js') !!}" ></script>
<script src="{!! asset('js/validate/jquery.validationEngine-en.js') !!}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://www.google.com/recaptcha/api.js?hl=en" async defer></script>

<script>
    $("button[type=submit]").on("click", function (e) {
        e.preventDefault();
        let valid = $(this).closest("form").validationEngine("validate");
        if(valid) {
            $(this).closest("form").submit();
        }
    });
</script>
@stack("scripts")
</body>
</html>
