@extends("inc.app")

@section('title', __('Page Not Found'))

@section("content")
    <section class="error_area">
        <div class="container flex">
            <div class="error_contain text-center">
                <h2 class="f_p f_400 w_color f_size_40">404</h2>
                <p class="w_color f_300">The page you are looking for could not be found!</p>
                <div class="text-center">
                    <a href="/" class="btn">GO TO HOMEPAGE</a>
                </div>
            </div>
        </div>
    </section>
@endsection
