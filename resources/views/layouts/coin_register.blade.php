@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Register Coin</h1>
            </div>
        </div>
    </section>

    <section class="sign_in_area bg_color sec_pad">
        <div class="container">
            <div class="sign_info">
                <div class="row">
                    <div class="col-12">
                        @include("inc.errors")
                        <div class="sign_info_content text_box text-justify mb_50" style="color:black;">
                            <p>This is where you may register your coin for free. This will always be a free service, but we do accept donations
                                (link/wallet address).</p>
                            <p>Please ensure that you describe your coin in as much detail as possible, as CoinRegistrar is where your investors will see
                                you are a registered developer, and see your coin info. The more details that are available to the investors, the better.</p>
                        </div>
                        <div class="col-10 offset-1 bordered_box float-left">
                            <a href="javascript:void(0);" id="badge">
                                <div class="col-lg-4 col-xs-12 float-left">
                                    <img src="{!! asset("img/devreg1.png") !!}" alt="Devreg" class="img-fluid"/>
                                </div>
                                <div class="col-lg-8 col-xs-12 float-left ">
                                    <div class="coin_logo"></div>
                                    <div class="float-left col-12 stars mt-4 mb-2 text-center"></div>
                                    <div class="float-left col-12 stars_info mt-1 text-center"></div>
                                </div>
                            </a>
                        </div>
                        <div class="clearfix"></div>
                        <div class="login_info mt_30">
                            <form action="{{ route("post_coin") }}" method="POST" enctype="multipart/form-data" class="login-form log-in-form apply_form">
                                @csrf
                                <input type="hidden" name="usr" value="{{encrypt(auth()->id())}}" />
                                <input type="hidden" name="coin_id" />
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Full Name Of Coin</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("name") !!}" name="name" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Select Type Of Coin</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("type") !!}" name="type" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Maximum Supply of Coins</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("supply") !!}" name="supply" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Github Source Link (Release is required)</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("github_link") !!}" name="github_link" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Official Coin Ticker (e.g BTC) If listed on CMC you must not choose an existing Ticker,
                                        Make sure your Ticker is Unique.</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("ticker") !!}" name="ticker" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">CoinMarketCap (CMC) preferred, if applicable, or LiveCoinWatch/CoinGecko
                                        Link</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("market_cap") !!}" name="market_cap" placeholder="https://...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">BitcoinTalk ANN Page</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("bitcoin_talk") !!}" name="bitcoin_talk" placeholder="https://...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">List the Asset/s your Project has Forked /Cloned from (Version Number)</label>
                                    <textarea type="text" class="validate[required]" name="assets" placeholder="Enter...">{!! old("assets") !!}</textarea>
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Official Twitter Account</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("twitter") !!}" name="twitter" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">What Base Chain is your Coin on (If Applicable, e.g Dash)</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("base_chain") !!}" name="base_chain" placeholder="Enter...">
                                </div>

                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Is this coin a PoW, PoS, Masternode or Staking</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("pow_pos") !!}" name="pow_pos" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">What Trading Exchanges is the coin on.</label>
                                    <textarea type="text" class="validate[required]" name="exchanges" placeholder="https://magnaex.io">{!! old("exchanges") !!}</textarea>
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Discord permanent Invite address</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("discord") !!}" name="discord" placeholder="https://discord.gg">
                                </div>

                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Link to Logo 250x250px, Transparent PNG required</label>
                                    <input type="text" class="validate[required]" autocomplete="off" value="{!! old("logo_link") !!}" name="logo_link" placeholder="https://...">
                                </div>

                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Upload logo to here</label>
                                    <input type="file" class="validate[required]" class="mt-0" name="logo" >
                                </div>

                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Link coin to Magnaex Trading exchange <br />(Free to
                                        registered users... Not required but its free.)</label>
                                    <input type="file" class="mt-0" name="magnaex">
                                </div>

                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">*Link coin information to coinregister.net (Required)</label>
                                    <input type="file" class="mt-0 validate[required]" name="coin_info" id="coininfo">
                                </div>

                                <div class="clearfix"></div>
                                <div class=" text-center">
                                    <ul class="list-unstyled mb-1" style="color:black">
                                        <li>You can tell Investors about your coin and this will go automatically be seen at coinregister.net</li>
                                        <li><a href="http://coinregistrer.org" target="_blank">Coinregister.net</a> will be free advertising for your coin, and we will be pushing the websites for you, In addition, we suggest you do the same at your end.</li>
                                        <li>As we partner up with other exchanges we will be asking them our Partners for discounts so the developers who register here may get onto their Trading Exchange at a discounted price</li>
                                        <li>Just another bonus for the registered developer</li>
                                    </ul>
                                </div>
                                <div class="extra offset-lg-4">
                                    {{--<div class="g-recaptcha" data-sitekey="6Lf4EJEUAAAAAIyN5xbkq52GCKvGcd9QoV_tAKec"></div>--}}
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn_three">REGISTER COIN</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@push("scripts")
    <script>
        $(document).ready(function () {
            $("[name=name]").on("change", function () {
                let coin_name = $(this).val();
                $.ajax({
                    url : "/check-coin",
                    type : "POST",
                    data : {coin_name : coin_name, _token : "{{ csrf_token() }}"},
                    success : function (data) {
                        let stars = "";
                        if( data.count > 0 ) {
                            if(data.count == 7) {
                                Swal.fire('Seven developers are already registered for this coin! Please register another coin!');
                                $("button[type=submit]").attr("disabled",true);

                                $("input").not("[name=name]").not("[name=_token]").not("[name=coin_id]").each(function(i,el){
                                    $(el).attr("disabled",true).val("");
                                });

                                $("textarea").each(function (i,el) {
                                    $(el).attr("disabled",true).text("");
                                });

                                for(let i = 1; i <= data.count; i++)
                                {
                                    stars += '<img style="display: block;margin-left: auto;margin-right: auto" class="mr-3 float-left" width="50" src="{!! asset("img/star.png") !!}" alt="Star"/>';
                                }
                                $('<small id="helptext" class="text-red form-text">This coin is already registered!</small>').insertAfter($("[name=name]"));
                                $(".coin_logo").html('<img style="display: block;margin-left: auto;margin-right: auto" width="150" src="' + data.logo +'" alt=""/>');
                                $(".stars").html(stars);
                                $(".stars_info").text('"Stars Represent Amount Of Registered Developers Per Coin"').css({"color": "black"});
                                $("#badge").attr("href", "https://coinregister.net/" + $("[name=name]").val()).attr("target","_blank");

                            } else {
                                for(let i = 1; i <= data.count; i++)
                                {
                                    stars += '<img style="display: block;margin-left: auto;margin-right: auto" class="mr-3 float-left" width="50" src="{!! asset("img/star.png") !!}" alt="Star"/>';
                                }
                                $('<small id="helptext" class="text-red form-text">This coin is already registered!</small>').insertAfter($("[name=name]"));
                                $(".coin_logo").html('<img style="display: block;margin-left: auto;margin-right: auto" width="150" src="' + data.logo +'" alt=""/>');
                                $(".stars").html(stars);
                                $(".stars_info").text('"Stars Represent Amount Of Registered Developers Per Coin"').css({"color": "black"});
                                $("#badge").attr("href", "https://coinregister.net/" + $("[name=name]").val()).attr("target","_blank");

                                $("input").not("[name=name]").not("[name=_token]").not("[name=coin_id]").each(function(i,el){
                                    $(el).attr("disabled",true).removeClass("validate[required]").val("");
                                });

                                $("textarea").each(function (i,el) {
                                    $(el).attr("disabled",true).removeClass("validate[required]").text("");
                                });
                                $("[name=coin_id]").val((data.id).toString());
                            }
                        } else {
                            $("#helptext").remove();
                            $("input").each(function (i,el) {
                                $(el).attr("disabled",false).addClass("validate[required]");
                            });
                            $("textarea").each(function (i,el) {
                                $(el).attr("disabled",false).addClass("validate[required]").text("");
                            });
                            $(".coin_logo").html("");
                            $(".stars").html("");
                            $(".stars_info").text("");
                            $("#badge").attr("href", "javascript:void(0);").removeAttr("target");
                            $("button[type=submit]").attr("disabled",false);
                        }
                    }
                });
            });
        });
    </script>
@endpush
