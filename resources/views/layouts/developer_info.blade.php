@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">CoinRegistrar</h1>
            </div>
        </div>
    </section>

    <section class="job_listing_area bg_color sec_pad">
        <div class="container">
            <img style="display: block;margin-left: auto;margin-right: auto" width="250" src="{!! asset($logo) !!}" alt=""/>
            <div class="job_listing">
                <div class="tab-content listing_tab mb-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="london" role="tabpanel" aria-labelledby="london-tab">
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Full Name</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $developer->fullname !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Date Of Birth</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $developer->extras->dateofbirth !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Country</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $developer->extras->country !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Discord Username</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $developer->extras->discord_name !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Preffered Language</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $developer->extras->language !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
