@extends("inc.app")

@section("content")
<section class="breadcrumb_area">
    <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
    <div class="container">
        <div class="breadcrumb_content text-center">
            <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">CoinRegistrer</h1>
        </div>
    </div>
</section>

<section class="sign_in_area bg_color sec_pad">
    <div class="container">
        <div class="sign_info">
            <div class="row">
                <div class="col-12">
                    <div class="login_info">
                        <h2 class="text-center f_p f_600 f_size_24 t_color3 mb_40">Log in to Your Account</h2>
                        @include("inc.errors")
                        <form action="{{ route("login") }}" class="login-form log-in-form" method="POST">
                            @csrf
                            <div class="form-group text_box">
                                <label class="f_p text_c f_400">Username</label>
                                <input type="text" name="username" class="validate[required]" value="{{ old("username") }}" placeholder="Enter your username">
                            </div>
                            <div class="form-group text_box">
                                <label class="f_p text_c f_400">Password</label>
                                <input type="password" name="password" class="validate[required]" placeholder="******">
                            </div>
                            <div class="extra mb_20">
                                <div class="float-right">Don't Have An Account? <a class="login-link" href="{{ url("/register") }}">Register</a></div>
                            </div>
                            <div class="offset-lg-4">
                                <div class="g-recaptcha" data-sitekey="6Lf4EJEUAAAAAIyN5xbkq52GCKvGcd9QoV_tAKec"></div>
                            </div>
                            <button type="submit" class="btn_three">LOGON</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
