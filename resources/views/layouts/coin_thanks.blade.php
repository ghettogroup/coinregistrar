@extends("inc.app")

@section("content")
    <section class="error_area">
        <div class="container flex">
            <div class="error_contain text-center">
                <h2 class="f_p f_400 w_color f_size_40">Coin is registered!</h2>
                <p class="w_color f_300">Thanks for registering your coin to CoinRegister.</p>
            </div>
        </div>
    </section>
@endsection
