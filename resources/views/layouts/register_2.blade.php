@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
    <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
    <div class="container">
        <div class="breadcrumb_content text-center">
            <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Complete Registration</h1>
        </div>
    </div>
    </section>

    <section class="sign_in_area bg_color sec_pad">
        <div class="container">
            <div class="sign_info">
                <div class="row">
                    <div class="col-12">
                        <div class="login_info">
                            <h2 class="text-center f_p f_600 f_size_24 t_color3 mb_40">Create and upload Your Account Final</h2>
                            @include("inc.errors")
                            <form action="{{ route("complete-register") }}" method="POST" class="login-form log-in-form apply_form" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Full Name</label>
                                    <input type="text" disabled name="fullname" value="{!! $user->fullname !!}" placeholder="Enter your full name">
                                    <input type="hidden" name="usr_tkn" value="{!! encrypt($user->id) !!}">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Date Of Birth</label>
                                    <input type="text" name="date_of_birth" value="{{ old("date_of_birth") }}" class="datepicker" placeholder="mm/dd/yyyy">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">City *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("city") }}" name="city" placeholder="Enter your city">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">State *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("state") }}" name="state" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Country of Residence *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("country") }}" name="country" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Zip or Postal Code *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("zip_postal_code") }}" name="zip_postal_code" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Mobile Phone Number *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("mobile_phone_number") }}" name="mobile_phone_number" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Skype User Name *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("skype_name") }}" name="skype_name" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Discord User Name *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("discord_name") }}" name="discord_name" placeholder="Enter...">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Preffered Language *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("language") }}" name="language" placeholder="Enter...">
                                </div>

                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Pasport ID Number *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("passport_id") }}" name="passport_id" placeholder="Enter...">
                                </div>
                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Passport Photo (biometric format), copy both sides *</label>
                                    <input type="file" class="mt-0 validate[required]" name="passport_photo">
                                </div>

                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Country ID Card Number *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("country_id_card_no") }}"  name="country_id_card_no" placeholder="Enter...">
                                </div>
                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Country ID Card copy both sides *</label>
                                    <input type="file" class="mt-0 validate[required]" name="country_id_card" >
                                </div>

                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Drivers License Number *</label>
                                    <input type="text" class=" validate[required]" value="{{ old("driver_licence_no") }}"   name="driver_licence_no" placeholder="Enter...">
                                </div>
                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Drivers License (Front and Back) *</label>
                                    <input type="file" class="mt-0 validate[required]" name="driver_licence" >
                                </div>

                                <div class="form-group upload_box">
                                    <label class="f_p f_400 mb-0 form-labe" style="">Self Picture Showing current Utility with date and address *</label>
                                    <input type="file" class="mt-0 validate[required]" name="photo" >
                                </div>


                                <div class="clearfix"></div>
                                <div class="sign_info_content text-center">
                                    <div class="text-center mb-2 mt-0">* Required</div>
                                    <div class="extra mb_20 offset-4">
                                        <div class="g-recaptcha" data-sitekey="6Lf4EJEUAAAAAIyN5xbkq52GCKvGcd9QoV_tAKec"></div>
                                    </div>
                                    <ul class="list-unstyled mb-1">
                                        <li>Please fill out this form in its entirety, as well as upload the required identifications in PDF format.</li>
                                        <li>Include your Skype, or Discord username to enable us to perform a live video interview with you.</li>
                                    </ul>
                                    <strong>P.O. Box Addresses are not accepted.</strong>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn_three">SUBMIT APPLICATION</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push("scripts")
    <script>
        $('.datepicker').datepicker();
    </script>
@endpush
