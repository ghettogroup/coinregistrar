@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Privacy Policy</h1>
            </div>
        </div>
    </section>
    <section class="blog_area sec_pad mt-5">
        <div class="container">
            <div class="row">
                <div>
                    <div class="blog_single mb_50">
                        <div class="blog_content text-justify">
                            <h3 class="f_p f_size_20 f_500 t_color mb-30">Description</h3>
                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Our Enforced Privacy Laws</h5>
                            <p><a href="http://devregistrar.org" target="_blank">Devregistrar.org</a> and Blockchain Management Corporation Pty Ltd Data Protection Policies
                                We are committed to taking all reasonable steps to ensure that our clients’ information will be
                                held securely.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Informed</h5>
                            <p>With privacy statements that are easy to find, written in clear and understandable language. Our
                                privacy statements clearly describe the extent and primary purposes for all collection and use of
                                personal information. Statement changes will occasionally be required and will be prominently
                                noted. Statements will explain what effect the changes have concerning client information and
                                will be dated to show when the policy is effective.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Provided Meaningful Choice</h5>
                            <p>We endeavor to provide easy-to-use information and update processes for clients.
                                All Information about clients is held strictly in utmost security and used only in the following
                                manner.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Able to Review Their Information</h5>
                            <p>We will refuse a request for access to personal information when permitted by law and, in the
                                unlikely event that this occurs, will give a written explanation for our decision.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Provided Security for your Data</h5>
                            <p>Through strong measures incorporated in a security program designed to protect clients
                                information from unauthorised access, use, or disclosure.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Technical Support</h5>
                            <p>If you contact our Support Line, or seek online support from us, we will hold your details. <br />
                                (1) Your name, address, contact number(s) and email address <br />
                                (2) Your IP address and computer name <br />
                                (3) your user name and badge number <br />
                                (4) To confirm your identity <br />
                                (5) To provide you with help you have requested <br />
                                (6) To follow up or pursue any queries you make. <br />
                                We generally keep a record of the problem that you report and any actions that we took.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Security</h5>
                            <p>We screen all our staff and inform them of their legal obligations in writing.
                                The security and privacy of all our clients is a priority.
                                No unauthorised person has access to our company records.
                                All records are for <a href="http://devregistrar.org" target="_blank">devregistrar.org®TM</a>  administration only.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Sharing</h5>
                            <p>We will not share your information with any third party, except where required by Law. Your
                                information will not be given, sold or otherwise distributed to any agencies, advertisers, partners
                                or others, unless required by Law.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
