@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
    <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
    <div class="container">
        <div class="breadcrumb_content text-center">
            <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">CoinRegistrer</h1>
        </div>
    </div>
    </section>
    <section class="sign_in_area bg_color sec_pad">
        <div class="container">
            <div class="sign_info">
                <div class="row">
                    <div class="col-lg-7">
                        <div class="login_info">
                            <h2 class="text-center f_p f_600 f_size_24 t_color3 mb_40">Create Your Account</h2>
                            @include("inc.errors")
                            <form action="{!! url("register") !!}" class="login-form log-in-form" method="POST">
                                @csrf
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Full Name</label>
                                    <input type="text" name="fullname" value="{{ old('fullname') }}" class="validate[required]" placeholder="Enter your full name">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Username</label>
                                    <input type="text" name="username" value="{{ old('username') }}" class="validate[required]" placeholder="Enter your username">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">E-mail Address</label>
                                    <input type="text" name="email" value="{{ old('email') }}" class="validate[required]" placeholder="Enter your e-mail address">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Password</label>
                                    <input type="password" name="password" class="validate[required]" placeholder="***********">
                                </div>
                                <div class="form-group text_box">
                                    <label class="f_p text_c f_400">Confirm Password</label>
                                    <input type="password" name="password_confirmation" class="validate[required]" placeholder="***********">
                                </div>
                                <div class="extra mb_20 offset-3">
                                    <div class="g-recaptcha" data-sitekey="6Lf4EJEUAAAAAIyN5xbkq52GCKvGcd9QoV_tAKec"></div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn_three mt-0 mb-2">Submit Enquiry</button>
                                </div>
                                <div class="float-right mt-2">Already Have An Account? <a class="login-link" href="{{ route("login") }}">Sign In</a></div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-5">
                        <div class="sign_info_content">
                            <h2 class="f_p f_600 f_size_20 t_color3 mb_40">PLEASE NOTE:</h2>
                            <p class="f_p f_300 f_size_20 mb-30 text-justify">All information supplied by you is kept in an off line server, At no time will your information be given to any Government
                                Department, (OR) Third Parties, Unless the following:</p>
                            <ul class="list-unstyled mb-0 text-justify">
                                <li><i class="ti-minus"></i>A Breach of the terms where you purposely defraud, steals, or is dishonest in obtaining funds by deception.</li>
                                <li><i class="ti-minus"></i>To this, we will release only under warrant for information to any law enforcement that issues us with one.</li>
                                <li><i class="ti-minus"></i>To this we will check with you via email, before the information is given out which you must reply within 3 days.</li>
                                <li class="f_p f_600 f_size_24 t_color3 mt_40 text-justify">WE WILL ENDEAVOUR TO KEEP YOUR INFORMATION SAFE AT ALL TIMES RE: OUR STRICT PRIVACY LAWS.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
