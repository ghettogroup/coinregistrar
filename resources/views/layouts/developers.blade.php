@extends("inc.app")

@section("content")

    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Developers</h1>
            </div>
        </div>
    </section>

    <section class="job_listing_area bg_color sec_pad">
        <div class="container">
            <div class="job_listing">
                <div class="tab-content listing_tab mb-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="london" role="tabpanel" aria-labelledby="london-tab">
                        @if($developers->count() > 0)
                            @foreach($developers as $developer)
                                <div class="list_item">
                                    <div class="joblisting_text">
                                        <div class="job_list_table">
                                            <div class="jobsearch-table-cell">
                                                <h4><a href="{!! url("/developer/" . encrypt($developer->id)) !!}" class="f_500 t_color3">{!! $developer->name !!}</a></h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @endif
                    </div>
                </div>

            </div>
        </div>
    </section>


@endsection
