@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">{!! $coin->name !!}</h1>
            </div>
        </div>
    </section>

    <section class="job_listing_area bg_color sec_pad">
        <div class="container">
            <img style="display: block;margin-left: auto;margin-right: auto" width="250" src="{!! asset($logo) !!}" alt=""/>
            <div class="job_listing">
                <div class="tab-content listing_tab mb-5" id="myTabContent">
                    <div class="tab-pane fade show active" id="london" role="tabpanel" aria-labelledby="london-tab">
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Type Of Coin</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->type !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Github Source Link</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->github_link !!}" target="_blank">{!! $coin->github_link !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Github Source Link</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->supply !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Official Coin Ticker</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->ticker !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>CoinMarketCap Link</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->market_cap !!}" target="_blank">{!! $coin->market_cap !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>BitcoinTalk ANN Page</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->bitcoin_talk !!}" target="_blank">{!! $coin->bitcoin_talk !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>The Asset/s the Project has Forked /Cloned from</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->assets !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Official Twitter Account</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->twitter !!}" target="_blank">{!! $coin->twitter !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Base Chain</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->base_chain !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Is this coin a PoW, PoS, Masternode or Staking</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->pow_pos !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>What Trading Exchanges is the coin on?</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p>{!! $coin->exchanges !!}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Discord permanent Invite address</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->discord !!}" target="_blank">{!! $coin->discord !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="list_item">
                            <div class="joblisting_text">
                                <div class="job_list_table">
                                    <div class="jobsearch-table-cell">
                                        <p>Link to Logo</p>
                                    </div>
                                    <div class="jobsearch-table-cell">
                                        <p><a href="{!! $coin->logo_link !!}" target="_blank">{!! $coin->logo_link !!}</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endsection
