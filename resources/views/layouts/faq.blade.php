@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">FAQ</h1>
            </div>
        </div>
    </section>
    <section class="blog_area sec_pad mt-3 mb-5">
        <div class="container">
            <div class="tab-content faq_content" id="myTabContent">
                <div class="tab-pane fade show active" id="purchas" role="tabpanel" aria-labelledby="purchas-tab">
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Is there a fee to register?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion" style="">
                                <div class="card-body">
                                    No. Registering is completely free.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                    <button class="btn btn-link" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                                        Why is the registration free? What catch to this is there?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion" style="">
                                <div class="card-body">
                                    The reason the registration is free is that developers will have no excuse to not register and
                                    prove legitimacy to their projects.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        How are developers verified when registering?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    Information is gathered, including, but not limited to, photos of Drivers License, Passport,
                                    Country ID and an amenity bill with current address. Once all information is submitted, you will
                                    be contacted to schedule a date and time for a live video conference call to complete the
                                    verification process.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfour">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                        What assurance can you provide with the security of the information given by the developers?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsefour" class="collapse" aria-labelledby="headingfour" data-parent="#accordion">
                                <div class="card-body">
                                    All information provided is stored in an encrypted, secure, offline server.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingfive">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                        Is the developer's personal information displayed, or kept private?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsefive" class="collapse" aria-labelledby="headingfive" data-parent="#accordion">
                                <div class="card-body">
                                    All personal information is kept private. No personal information is presented to or given to
                                    anyone. All personal information is kept on a secure, offline server.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingsix">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsesix" aria-expanded="false" aria-controls="collapsesix">
                                        What happens if a verified developer is found to have committed fraud?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsesix" class="collapse" aria-labelledby="headingsix" data-parent="#accordion">
                                <div class="card-body">
                                    <p>The developer will lose their certification and will be blacklisted from ever gaining
                                        registration again from <a href="http://devregistrar.org" target="_blank">DevRegistrar.org</a>.</p>

                                    <p>If any government issues us a warrant to obtain a developers information as a result of a scam, or
                                    fraudulent act, we will contact that developer before releasing the information. The developer
                                    has 3 days in which to respond to us. After 3 days, we will hand over any/all requested
                                        information to the authorities.</p>
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingseven">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseseven" aria-expanded="false" aria-controls="collapseseven">
                                        Does Dev Registrar have any protection against re-registration of a fraudulent developer?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseseven" class="collapse" aria-labelledby="headingsix" data-parent="#accordion">
                                <div class="card-body">
                                    Yes. Any developer that is blacklisted will not be able to re-register. Their information will
                                    be kept on file, and they will not be able to use the same information. In addition, the live video
                                    conference verifies that the developer is who they say they are, based on physical examination of
                                    the photo documents supplied by them during the registration process.
                                </div>
                            </div>
                        </div>

                        <div class="card">
                            <div class="card-header" id="headingeight">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseeight" aria-expanded="false" aria-controls="collapseeight">
                                        Is the developer's information safe from others?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapseeight" class="collapse" aria-labelledby="headingeight" data-parent="#accordion">
                                <div class="card-body">
                                    Yes, all developers who sign up are covered under strict privacy laws, and stored on a
                                    secure, offline server.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="headingnine">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapsenine" aria-expanded="false" aria-controls="collapsenine">
                                        Are we able to list our coins onto the coin register?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapsenine" class="collapse" aria-labelledby="headingnine" data-parent="#accordion">
                                <div class="card-body">
                                    Yes. When you fill out the form to list all the information on your coin in DevRegister.org,
                                    this information is automatically sent to CoinRegistrar.net.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading10">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse10" aria-expanded="false" aria-controls="collapse10">
                                        What is the badge for?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse10" class="collapse" aria-labelledby="heading10" data-parent="#accordion">
                                <div class="card-body">
                                    The badge is your personal proof of verification/registration with DevRegistrar.org. The
                                    badge can be displayed on your website and other social media to show your investors that you
                                    have been registered with DevRegistrar.org.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading11">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse11" aria-expanded="false" aria-controls="collapse11">
                                        What do the stars on the badge represent?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse11" class="collapse" aria-labelledby="heading11" data-parent="#accordion">
                                <div class="card-body">
                                    Each developer on your project that registers with DevRegistrar.org can be added to your
                                    badge as Stars. If your project has 6 developers running the coin/project, the lead developer will
                                    have the badge, and a Star will be added for each of your registered developers on your team.
                                    Your badge would then be equipped with 5 Stars, representing the other 5 developers.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading12">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse12" aria-expanded="false" aria-controls="collapse12">
                                        If I have more than one developer on my team and I want them removed from the coin
                                        register, can I remove that developer from our registered coin?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse12" class="collapse" aria-labelledby="heading12" data-parent="#accordion">
                                <div class="card-body">
                                    Yes. You may report that you want a developer removed to the DevRegistrar.org Admin,
                                    who will then remove a developer Star from your coin listing.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading13">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse13" aria-expanded="false" aria-controls="collapse13">
                                        What is CoinRegistrar.net?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse13" class="collapse" aria-labelledby="heading13" data-parent="#accordion">
                                <div class="card-body">
                                    CoinRegistrar.net is a site where investors can come and see a list of coins and projects
                                    developers have registered with DevRegistrar.org. The developer can upload the coin's/project's
                                    information/details for all potential investors to see. Not only does this give investors more faith
                                    in your coin, but it allows potential investors to see the coin and research it.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading14">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse14" aria-expanded="false" aria-controls="collapse14">
                                        What if my coin fails and dies off? Or want to abandon it:<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse14" class="collapse" aria-labelledby="heading14" data-parent="#accordion">
                                <div class="card-body">
                                    <p>You must report to the DevRegistrar.org Admin with the reasons why you want to abandon
                                    the coin. The coin will be added to the "To Be Delisted" list for one week to allow the investors
                                        to see that the coin is being deactivated.</p>
                                    <p>Once the delisting request is excepted by the DevRegistrar.org Admin, the developer will remain
                                        registered and will be able to continue as a registered developer for other coins.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading15">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse15" aria-expanded="false" aria-controls="collapse15">
                                        What is the "To Be Delisted" list?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse15" class="collapse" aria-labelledby="heading15" data-parent="#accordion">
                                <div class="card-body">
                                    This is a list of coins that are scheduled to be delisted from CoinRegistrar.net, per the
                                    request of the developer, or if a coin is found to be fraudulent. Coins will remain on the list for 1
                                    week, to allow the investors to see that the coin is being deactivated.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading16">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse16" aria-expanded="false" aria-controls="collapse16">
                                        Can a registered Developer takeover a dead, or abandoned coin that is still active on
                                        CoinRegistrar.net?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse16" class="collapse" aria-labelledby="heading16" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Yes, a developer may take over an old coin. The new developer taking over a project must
                                    contact the DevRegistration.org Admin and inform him. Contact will then be made with the
                                    current developer of the project to ensure the takeover is legitimate.</p>
                                    <p>Once the original developer agrees to the takeover, the old developer will be removed from the
                                        coin listing, and the new developer will be added.</p>
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading17">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse17" aria-expanded="false" aria-controls="collapse17">
                                        I am taking over a coin/project, but I don't have access to the original social media accounts
                                        of the old developers. What do I do?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse17" class="collapse" aria-labelledby="heading17" data-parent="#accordion">
                                <div class="card-body">
                                    You will need to create new accounts, and provide us with the new links to those pages. We
                                    will then update the coin listing with the new information.
                                </div>
                            </div>
                        </div>
                        <div class="card">
                            <div class="card-header" id="heading18">
                                <h5 class="mb-0">
                                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse18" aria-expanded="false" aria-controls="collapse18">
                                        Can an unregistered developer obtain the login passwords for another developer?<i class="ti-plus"></i><i class="ti-minus"></i>
                                    </button>
                                </h5>
                            </div>
                            <div id="collapse18" class="collapse" aria-labelledby="heading18" data-parent="#accordion">
                                <div class="card-body">
                                    At no time will the login information be handed out to an unregistered developer, or any
                                    other registered developer. We will gladly accept a new developers registration if they want to
                                    register at Devregistrar.org.
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
    </section>
@endsection
