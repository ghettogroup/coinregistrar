@extends("inc.app")

@section("content")
    <section class="error_area">
        <div class="container flex">
            <div class="error_contain text-center">
                <h2 class="f_p f_400 w_color f_size_40">Thanks for Registering!</h2>
                <p class="w_color f_300">Please follow the instructions on your mail address to complete your registration.</p>
            </div>
        </div>
    </section>
@endsection
