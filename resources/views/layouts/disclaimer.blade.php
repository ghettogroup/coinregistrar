@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color">Disclaimer</h1>
            </div>
        </div>
    </section>
    <section class="blog_area sec_pad mt-5">
        <div class="container">
            <div class="row">
                <div>
                    <div class="blog_single mb_50">
                        <div class="blog_content text-justify">
                            <h5 class="f_p f_size_20 f_500 t_color mb-30">Disclaimer Agreement</h5>
                            <p> In this Terms of Use Agreement & Disclaimer, and throughout the agreements on this
                                Website, (“Agreement”)
                                “you” and “your” refer to each customer or website visitor, “we”, “us” and “our” refer
                                to Blockchain Management
                                Corporation Pty Ltd, (doing business as BMC and BMS a registered company namely
                                Blockchain Management
                                Corporation Pty Ltd), its contractors, agents, employees, officers, directors and
                                affiliates (hereinafter “BMC or
                                BMS”) and “Services” refers to all services provided by us.
                                BMC PROVIDES THE WEBSITE LOCATED AT <a href="http://www.devregistrar.org"
                                                                       target="_blank">http://www.devregistrar.org</a>,
                                <a href="http://www.devregister.org" target="_blank">http://www.devregister.org</a> ,
                                <a href="http://www.devreg.org" target="_blank">http://www.devreg.org</a>, <a
                                    href="https://coinregistrar.com" target="_blank">https://coinregistrar.com</a>, <a
                                    href="https://coinregister.net" target="_blank">https://coinregister.net</a>, <a
                                    href="https://coinregistrar.net" target="_blank">https://coinregistrar.net</a>,
                                <a href="http://www.registerdevs.com" target="_blank">http://www.registerdevs.com</a>,
                                <a href="http://www.registerdevelopers.com" target="_blank">http://www.registerdevelopers.com</a>,
                                <a href="http://www.developerregister.com" target="_blank">http://www.developerregister.com</a>
                                (the “Site”)
                                AND RELATED SERVICES SUBJECT TO YOUR COMPLIANCE WITH THE TERMS AND CONDITIONS
                                SET FORTH HEREIN. PLEASE READ THE FOLLOWING INFORMATION CAREFULLY. THIS IS A
                                LEGAL AGREEMENT BETWEEN YOU AND BLOCKCHAIN MANAGEMENT CORPORATION. BY
                                USING THIS SITE, YOU AGREE THAT YOU HAVE READ AND UNDERSTAND THE TERMS AND
                                CONDITIONS OF THIS AGREEMENT AND THAT YOU AGREE TO BE RESPONSIBLE FOR EACH AND
                                EVERY TERM AND CONDITION.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">DISCLAIMER</h5>
                            <p>THIS SITE IS PROVIDED AS-IS WITH NO REPRESENTATIONS OR WARRANTIES, EITHER EXPRESS
                                OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, IMPLIED WARRANTIES OF MERCHANTABILITY,
                                FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. YOU ASSUME COMPLETE
                                RESPONSIBILITY AND RISK FOR USE OF THIS SITE AND ANY AND ALL SITE-RELATED SERVICES.
                                Some jurisdictions do not allow the exclusion of implied warranties, so the above
                                exclusion may not apply to you.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">LIMITATION OF LIABILITY</h5>
                            <p>BLOCKCHAIN MANAGEMENT CORPORATION, ITS AGENTS, REPRESENTATIVES AND EMPLOYEES
                                ARE NEITHER RESPONSIBLE NOR LIABLE TO YOU OR YOUR BUSINESS FOR ANY DIRECT,
                                INDIRECT, INCIDENTAL, CONSEQUENTIAL, SPECIAL, EXEMPLARY, PUNITIVE, LOST OR IMPUTED
                                PROFITS OR ROYALTIES OR OTHER DAMAGES WHATSOEVER OUT OF OR RELATING IN ANY
                                WAY TO THIS SITE, SITE-RELATED SERVICES AND/OR CONTENT OR INFORMATION CONTAINED
                                WITHIN THE SITE, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS,
                                BUSINESS INTERRUPTION, COSTS OF PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES OR
                                FOR ANY CLAIM AGAINST YOU BY ANY OTHER PARTY, OR OTHER PECUNIARY LOSS.
                                BLOCKCHAIN MANAGEMENT CORPORATION SHALL NOT BE LIABLE TO YOU WHETHER FOR
                                BREACH OF WARRANTY OR ANY OBLIGATION ARISING THEREFROM OR OTHERWISE, WHETHER
                                LIABILITY IS ASSERTED IN CONTRACT OR TORT (INCLUDING NEGLIGENCE AND STRICT
                                PRODUCT LIABILITY) AND IRRESPECTIVE OF WHETHER YOU HAVE BEEN ADVISED OF THE
                                POSSIBILITY OF ANY SUCH LOSS OR DAMAGE. YOU HEREBY WAIVE ANY CLAIMS THAT THESE
                                EXCLUSIONS DEPRIVE YOU OF AN ADEQUATE REMEDY.</p>
                            <p>IN NO EVENT WILL BLOCKCHAIN MANAGEMENT CORPORATION AGGREGATE LIABILITY FOR
                                ANY AND ALL CLAIMS RELATING TO THIS SITE AND/OR THE GOODS AND SERVICES OFFERED IN
                                CONNECTION THEREWITH, WHETHER IN CONTRACT, TORT OR ANY OTHER THEORY OF
                                LIABIITY, EXCEED THE FEES PAID BY YOU FOR THE GOODS AND/OR SERVICES.</p>
                            <p>YOU ACKNOWLEDGE THAT THIRD PARTY PRODUCT AND SERVICE PROVIDERS MAY ADVERTISE
                                THEIR PRODUCTS AND SERVICES ON THE HTTP://BLOCKCHAINMANAGEMENTCORPORATION
                                WEB SITE. BLOCKCHAIN MANAGEMENT SERVICES FORMS “PARTNERSHIPS” OR ALLIANCES
                                WITH SOME OF THESE VENDORS FROM TIME TO TIME IN ORDER TO FACILITATE THE PROVISION
                                OF THESE PRODUCTS AND SERVICES TO YOU. HOWEVER, YOU ACKNOWLEDGE AND AGREE
                                THAT AT NO TIME IS BLOCKCHAIN MANAGEMENT CORPORATION MAKING ANY
                                REPRESENTATION OR WARRANTY REGARDING ANY THIRD PARTY’S PRODUCTS OR SERVICES,
                                NOR WILL BLOCKCHAIN MANAGEMENT SERVICES BE LIABLE TO YOU OR ANY THIRD PARTY
                                FOR ANY CLAIMS ARISING FROM OR IN CONNECTION WITH SUCH THIRD PARTY PRODUCTS AND
                                SERVICES. YOU HEREBY DISCLAIM AND WAIVE ANY RIGHTS AND CLAIMS YOU MAY HAVE
                                AGAINST BLOCKCHAIN MANAGEMENT CORPORATION WITH RESPECT TO THIRD PARTY
                                PRODUCTS AND SERVICES, TO THE MAXIMUM EXTENT PERMITTED BY LAW.</p>
                            <p>You understand and agree that any material and/or data downloaded or otherwise obtained
                                through the use of our
                                Services is done at your own discretion and risk and that you will be solely responsible
                                for any damage to your
                                computer system or loss of data that results from the download and/or use of such
                                material and/or data. You
                                understand that any documents that are downloaded from our site are downloaded at your
                                sole discretion and risk.
                                We make no warranty regarding any goods or services purchased or obtained or any
                                transactions entered into with
                                Blockchain Management Services. No advice or information, whether oral or written,
                                obtained by you from
                                Blockchain Management Corporation shall create any warranty not expressly made herein.
                                Some jurisdictions do
                                not allow the exclusion of certain warranties, so some of the above exclusions may not
                                apply to you.</p>
                            <p>BLOCKCHAIN MANAGEMENT CORPORATION at its sole discretion may choose to change the terms,
                                conditions and operation of this site at anytime. By using this service you waive any
                                rights or claims you may have
                                against BLOCKCHAIN MANAGEMENT CORPORATION.</p>
                            <p>The content available through the Site is the sole property of Blockchain Management
                                Corporation or its licensors
                                and is protected by copyright, trademark and other intellectual property laws. Except as
                                otherwise explicitly
                                agreed in writing, Blockchain Management Corporation-owned content received through the
                                Site may be
                                downloaded, displayed, reformatted and printed for your personal, non-commercial use
                                only. Content owned by
                                Blockchain Management Corporation licensors may be subject to additional restrictions.
                                You agree not to
                                reproduce, retransmit, distribute, disseminate, sell, publish, broadcast or circulate
                                the content received through the
                                Site to anyone, including but not limited to others in the same company or organisation
                                without Blockchain
                                Management Services express prior written consent.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">CUSTOMER SERVICE AGREEMENT</h5>
                            <p>This contract is governed by the law of all States and Territories in the Commonwealth of
                                Australia.
                                By entering into this contract, you (the customer) voluntarily adopt this governing law
                                and submit to the non-
                                exclusive jurisdiction of the courts and
                                tribunals of the said jurisdiction.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">THIS SITE IS NOT A SUBSTITUTE FOR LEGAL COUNSEL</h5>
                            <p>Blockchain Management Services is an internet publishing and help service. All materials
                                accessible via the Site
                                are intended to provide you with a convenient method for completing and filing your
                                requested forms.
                                The materials available at the Site contain information of general application and are
                                not intended to replace the advice
                                of an attorney. While our staff expends great efforts to maintain and publish accurate
                                information, State and
                                Federal laws are dynamic and constantly evolving. In addition, laws are open to
                                different interpretation and greatly
                                vary amongst different jurisdictions.</p>
                            <p>The materials, information and links posted on the Site are provided for public
                                informational purposes only, and
                                do not constitute individualised legal advice. The information on the Site is only
                                provided with the understanding
                                that Blockchain Management Corporation and its affiliates are not engaged in rendering
                                legal or other professional
                                services. Blockchain Management Corporation expressly disclaims any liability, loss or
                                risk incurred as a
                                consequence, directly or indirectly, of the use and application of any of the contents
                                of this information. This
                                information is not a substitute for the advice of a competent legal or other
                                professional. You are advised to consult
                                with qualified legal counsel to determine the current law and how it may apply to your
                                particular situation.</p>
                            <p>When using our Services, you will be acting as your own attorney. Blockchain Management
                                Corporation
                                completes information on the requested forms based upon the information you have
                                provided to us. By providing
                                you with this service, Blockchain Management Corporation, its advisors, agents,
                                representatives, and employees
                                are not rendering any legal or otherwise professional advice or service, and no
                                representations or warranties,
                                express or implied, are given regarding the legal or other consequences resulting from
                                the use of our Services,
                                including but not limited to information, content and/or forms.</p>
                            <p>Blockchain Management Corpoarion, its advisors, agents, representatives, and employees
                                are not engaged in the
                                practice of law and cannot provide you with legal advice. Although Blockchain Management
                                Corporation expends
                                great efforts and respects the confidential nature of the information you are submitting
                                to us, NO SPECIAL
                                RELATIONSHIP or privilege exists between Blockchain Management Corporation and you,
                                including but not
                                limited to any Attorney-Client relationship that might exist had you consulted with a
                                licensed attorney.</p>
                            <p>As with all important business matters, Blockchain Management Corporation, its advisors,
                                agents, representatives,
                                and employees STONGLY RECOMMEND that you consult with an attorney licensed to practice
                                law or a licensed
                                accountant in the applicable jurisdiction.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">STATE AND FEDERAL FILING REQUIREMENTS</h5>
                            <p>Blockchain Management Corporation is not responsible for advising or reminding you of any
                                requirements or
                                obligations, including, but not limited to any required State or Federal filings, annual
                                reports, taxes due, or other
                                filing requirements. Blockchain Management Corporation sole responsibility is the
                                preparation of your requested
                                form. Any requirements or obligations for the maintenance of your corporation, business
                                entity or other business
                                services are NOT the responsibility of Blockchain Management Corporation and are the
                                sole responsibility of you.
                                We strongly suggest that you send all correspondence to third parties via Certified
                                Return Receipt Mail.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mt-30">DISSATISFACTION</h5>
                            <p>In the event of dissatisfaction, Blockchain Management Corporation will undertake its
                                best efforts to resolve the
                                matter to your satisfaction.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">LINK DISCLAIMER</h5>
                            <p>Links to other sites are provided only as a courtesy you. These links do not constitute
                                an endorsement of products,
                                services or information provided by other sites or third parties. Further, the inclusion
                                of links to other sites does
                                not imply that the other sites have given permission for inclusion of these links, or
                                that there is any relationship
                                between Blockchain Management Corporation and the linked site. Blockchain Management
                                Services is not
                                responsible for the privacy practices or the content of others web sites. Blockchain
                                Management Corporation
                                hereby disclaims any and all representations or warranties expressed on any site other
                                than our own.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">COPYRIGHT</h5>
                            <p>All content within this Site, including, but not limited to text, software, graphics,
                                logos, icons and images are the
                                property of Blockchain Management Services. Except as provided herein, no portion of the
                                materials on these
                                pages may be reprinted or republished in any form without the express written permission
                                of Blockchain
                                Management Corporation. Permission is granted to print copies of informational articles
                                for your own use and
                                review, provided that source attributions and copyright notices are maintained.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">INTELLECTUAL PROPERTY</h5>
                            <p>Blockchain Management Corporation reserves all of its rights in the graphic image and
                                text, any other images, its
                                trade names and trademarks, copyrights and any and all intellectual property rights.
                                Blockchain Management
                                Corporation trade names, trademarks, logos and service names and similar proprietary
                                marks shall not be reprinted
                                or displayed in any form without the express written permission of Blockchain Management
                                Corporation. The
                                unique trade dress of the Site is also a service mark of Blockchain Management
                                Corporation.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">EMAIL TRANSMISSIONS</h5>
                            <p>Our site uses an online secure order form for customers to apply for our Services. When
                                you send Blockchain
                                Management Corporationan electronic mail transmission (as distinguished from a secure
                                order form, which is
                                encrypted), the electronic mail transmission is not necessarily secure and is not
                                encrypted. Accordingly, email
                                transmissions are not necessarily protected from unauthorised access. Transmission of
                                email is at your own risk.
                                Blockchain Management Corporation cannot accept responsibility for your transmission of
                                confidential
                                information or any obligation with respect to that information.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">SUBMISSION OF CORRECT INFORMATION BY YOU</h5>
                            <p>You agree that you are responsible for the spelling and other information and/or
                                corporate information forwarded
                                to Blockchain Management Services. You agree that you have double-checked all
                                information provided to
                                Blockchain Management Corporation prior to forwarding and that all information provided
                                to Blockchain
                                Management Corporation is exactly as you desire for Blockchain Management Corporation to
                                perform the
                                Services requested. You understand that the request for Services with the information
                                you provide is not reversible
                                after you submit your request. Submission of information via our website, via facsimile,
                                or otherwise authorizes
                                Blockchain Management Corporation to kep this information in a secure off line
                                computer</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">RIGHT OF REFUSAL</h5>
                            <p>Blockchain Management Corporation, in its sole discretion, reserves the right to refuse
                                to provide Services to you.
                                You agree that Blockchain Management Corporation shall not be liable to you for loss or
                                damages that may result
                                from our refusal to provide Services.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">SEVERABILITY</h5>
                            <p>In the event that any of the provisions of this Agreement are held to be unenforceable,
                                such provisions shall be
                                limited or eliminated to the minimum extent necessary so that this Agreement shall
                                otherwise remain in full force
                                and effect.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">COMPLIANCE WITH APPLICABLE LAWS</h5>
                            <p>You agree that you are in compliance with all applicable laws and regulations pertaining
                                to or governing your use
                                of this Site, and you agree to indemnify and hold Blockchain Management Corporation
                                harmless from and against
                                any and all claims, damages, losses or obligations suffered or incurred by you arising
                                from your failure to comply.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">WAIVER</h5>
                            <p>No term or provisions hereof shall be deemed waived and no breach excused, unless such
                                waiver or consent shall
                                be in writing and signed by the party claimed to have waived or consented. Any consent
                                by any party to, or waiver
                                of a breach by the other, whether express or implied, shall not constitute consent to,
                                waiver of, or excuse for any
                                other different or subsequent breach.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">ENTIRE AGREEMENT</h5>
                            <p>You agree that this Agreement amounts to the complete and exclusive agreement between you
                                and us regarding
                                our Services. This Agreement supersedes any prior agreements and understandings, whether
                                oral or written and
                                whether established by custom, practice, policy or precedent.</p>

                            <h5 class="f_p f_size_20 f_500 t_color mb-30">MODIFICATION</h5>
                            <p>Blockchain Management Corporation may modify this Agreement at any time by making such
                                modification on this page.</p>
                            <p>BY USING THIS SITE AND/OR ANY SERVICES RELATED THERETO, YOU AGREE THAT YOU HAVE
                                READ, UNDERSTAND AND AGREE TO THE TERMS OF USE AND DISCLAIMER.
                                –Thank you for choosing Blockchain Management Corporation. as your registering service.
                                Please feel free to
                                contact us with any questions or concerns you may have.
                                Writing to the address below:</p>
                            <p>Administration Manager
                                <br/>Blockchain Management Corporation Pty Ltd
                                <br/>11 Alick Road
                                <br/>Tottenham
                                <br/>Victoria 3012
                                <br/>Australia
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
