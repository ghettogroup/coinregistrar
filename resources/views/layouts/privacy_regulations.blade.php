@extends("inc.app")

@section("content")
    <section class="breadcrumb_area">
        <img class="breadcrumb_shap" src="{!! asset("img/breadcrumb/banner_bg.png") !!}" alt="">
        <div class="container">
            <div class="breadcrumb_content text-center">
                <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">Privacy Regulations</h1>
            </div>
        </div>
    </section>
    <section class="blog_area sec_pad mt-5">
        <div class="container">
                <div class="blog_single mb_50">
                    <iframe src="//docs.google.com/viewer?url=https://www.gyoder.org.tr/uploads/GYODER_PDFtuzuk.pdf&embedded=true" width="100%" height="600" style="border: none;"></iframe>
                </div>
            </div>
    </section>
@endsection
